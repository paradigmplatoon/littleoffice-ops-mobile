import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Observable } from 'rxjs/Observable';
import { SitesSelectionPage } from '../sites-selection/sites-selection';
/**
 * Generated class for the AuditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-audit',
  templateUrl: 'audit.html',
})
export class AuditPage {
  staffPic: string;
  isShowPic = false;
  timerVar: any;
  timerVal: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public camera: Camera) {
    this.startTimer();
  }
  startTimer(){
    this.timerVar=Observable.interval(1000).subscribe(x =>{
      this.timerVal = x;
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AuditPage');
  }
  TakePic() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320
    }).then((data) => {
      this.staffPic = "data:image/jpeg;base64," + data;
      this.isShowPic = true;
    }, (error) => {
    });
  }
  onClickTask(){
    this.navCtrl.push(SitesSelectionPage);
  }
}
