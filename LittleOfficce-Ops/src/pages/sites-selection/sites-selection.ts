import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddtaskPage } from '../addtask/addtask';
import { AuditPage } from '../audit/audit';
import { TrainingPage } from '../training/training';
import { DeploymentPage } from '../deployment/deployment';
import { MeetingPage } from '../meeting/meeting';
import { GeneralPage } from '../general/general';
import { ManPowerSearchPage } from '../man-power-search/man-power-search';
import { PurchagePage } from '../purchage/purchage';
import { OthersPage } from '../others/others';
import { HistoryPage } from '../history/history';
import { PickupPage } from '../pickup/pickup';
import { ViewTaskPage } from '../view-task/view-task';

/**
 * Generated class for the SitesSelectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sites-selection',
  templateUrl: 'sites-selection.html',
})
export class SitesSelectionPage {
  tasklist: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SitesSelectionPage');
  }
 
  onClickTask(){
    this.navCtrl.push(ViewTaskPage);
  }

  onClickAddTask(){
    this.navCtrl.push(AddtaskPage);
  }
  
}
