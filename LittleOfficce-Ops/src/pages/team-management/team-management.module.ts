import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamManagementPage } from './team-management';

@NgModule({
  declarations: [
    TeamManagementPage,
  ],
  imports: [
    IonicPageModule.forChild(TeamManagementPage),
  ],
})
export class TeamManagementPageModule {}
