import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SitesSelectionPage } from '../sites-selection/sites-selection';
import { ScheduleManagementPage } from '../schedule-management/schedule-management';
import { SetUpPage } from '../set-up/set-up';
import { FindMyTeamPage } from '../find-my-team/find-my-team';
import { TeamManagementPage } from '../team-management/team-management';
import { PracticePage } from '../practice/practice';
import { AddSitePage } from '../add-site/add-site';
import { AddZonePage } from '../add-zone/add-zone';

/**
 * Generated class for the HomeScreenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home-screen',
  templateUrl: 'home-screen.html',
})
export class HomeScreenPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeScreenPage');
  }
  onClickTask(){
    this.navCtrl.push(SitesSelectionPage);
  }
  onClickSchedule(){
    this.navCtrl.push(ScheduleManagementPage);
  }
  onClickSetup(){
    this.navCtrl.push(PracticePage);
  }
  onClickFindteam(){
    this.navCtrl.push(FindMyTeamPage)
  }
  onClickTeam(){
    this.navCtrl.push(TeamManagementPage)
  }
}
