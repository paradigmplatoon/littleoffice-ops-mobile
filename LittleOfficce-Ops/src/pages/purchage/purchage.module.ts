import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PurchagePage } from './purchage';

@NgModule({
  declarations: [
    PurchagePage,
  ],
  imports: [
    IonicPageModule.forChild(PurchagePage),
  ],
})
export class PurchagePageModule {}
