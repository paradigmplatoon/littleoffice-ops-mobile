import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScheduleManagementPage } from './schedule-management';

@NgModule({
  declarations: [
    ScheduleManagementPage,
  ],
  imports: [
    IonicPageModule.forChild(ScheduleManagementPage),
  ],
})
export class ScheduleManagementPageModule {}
