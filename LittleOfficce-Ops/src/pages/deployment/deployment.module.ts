import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeploymentPage } from './deployment';

@NgModule({
  declarations: [
    DeploymentPage,
  ],
  imports: [
    IonicPageModule.forChild(DeploymentPage),
  ],
})
export class DeploymentPageModule {}
