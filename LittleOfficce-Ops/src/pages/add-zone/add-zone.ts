import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddSitePage } from '../add-site/add-site';

/**
 * Generated class for the AddZonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
var map;
@IonicPage()
@Component({
  selector: 'page-add-zone',
  templateUrl: 'add-zone.html',
})
export class AddZonePage {
  isaddZone = false;
  regZone = false;
  isaddSite = false;
  isAp = false;
  isAddress = false;
  isCoords= false;
  category: string;
  multiZones: any;
  apName: any;
  SiteList: any;
  blockName: any;
  address: any;
  coordinates: any;
  name: any;
  user : any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    var latlng = new google.maps.LatLng(39.305, -76.617);
    map = new google.maps.Map(document.getElementById('map'), {
    center: latlng,
    zoom: 12
});
  }
  onClickZone(item){

  }
  onclickRegistered(zoneCat:string){
    this.category = zoneCat;
    this.isaddZone=!this.isaddZone
  }
  onclickAddSiteData(Sitecat:string){
    this.category = Sitecat;
    this.isaddSite = !this.isaddSite;
  }
  onChangeCoords(coords){

  }
  onChangeSite(site){

  }
  onClickSiteCancel(){

  }
  onChangeAddress(){

  }
  onClickAddSite(){
    this.navCtrl.push(AddSitePage);
  }
}
