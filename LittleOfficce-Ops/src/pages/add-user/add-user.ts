import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';


/**
 * Generated class for the AddUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-user',
  templateUrl: 'add-user.html',
})
export class AddUserPage {

  staffPic: string;
  name: string;
  mobile: string;
  design_lev: string;
  email: string;
  doj: string;
  dob: string;
  isShowPic = false;


  constructor(
    public navCtrl: NavController, 
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public api: ApiProvider,
    public store: Storage,
    public camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddUserPage');
  }

  takePic(){

    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320,
    }).then((data) =>{
      this.staffPic = "data:image/jpeg;base64," + data;
      this.isShowPic = true;
    }, (error) => {
    });
    
  }

  onClickSubmit(){

    if (!this.staffPic) {
      const toast = this.toastCtrl.create({
        message: 'Add the photo',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return false;
    }

    /* this.api.adduser(this.name, this.mobile, comp_id, this.design_lev, designation, zone_id, site_id, this.doj, this.dob, this.email, user_id, div_id, web_access, this.staffPic).then(res=>{

    }) */

  }

}
