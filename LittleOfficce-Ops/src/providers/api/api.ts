import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  apiUrl = 'https://www.littleoffice.co.in/paradigmops/v0';
  constructor(public http: Http)
  {
  }
  //users login Apis
  adduser(userName:string,mobileNumber:string,cmpId:string,userType:string,designation:string,zoneId:string,siteId:string,doj:string,dob:string,emaild:string,userId:string,divisionId:string,webAccess:string){
    return this.http.get(this.apiUrl + '/opsuser/adduser?userName='+userName +'&mobileNumber='+ mobileNumber +'&companyId='+cmpId+'&userType='+userType+'&designation='+designation+'&zoneId='+zoneId+'&doj='+doj+'&dob='+dob+'&emailId='+emaild+'&userId='+userId+'&siteId='+siteId+'&divisionId='+divisionId+'&webAccess='+webAccess).toPromise();
  }
  deleteUser(deleteUserId:string,cmpId:string,userId:string){
    return this.http.get(this.apiUrl + '/opsuser/deleteuser?deletingUserId='+deleteUserId+'&companyId='+cmpId+'&userId='+userId).toPromise();
  }
  reactivateUser(userId:string,reactivateId:string,cmpId:string){
    return this.http.get(this.apiUrl + '/opsuser/reactivateuser?userId='+userId+'&reactivateUserId='+reactivateId+'&companyId='+cmpId).toPromise();
  }
  updateUser(userId:string,userName:string,designation:string,emailId:string,webAccess:string){
    return this.http.get(this.apiUrl + '/opsuser/edituserdetails?userId='+userId+'&userName='+userName+'&designation='+designation+'&emailId='+emailId+'&webAccess='+webAccess).toPromise();
  }
  //login Api
  userLogin(emailId:string,password:string){
      return this.http.get(this.apiUrl + '/user/opslogin?emailId='+emailId+'&password='+password).toPromise();
  }
  //Zone Api
  addZone(name:string,cmpId:string,userId:string){
    return this.http.get(this.apiUrl + '/zone/addzone?name='+name+'&companyId='+cmpId+'&userId='+userId).toPromise();
  }
  zoneList(zoneId:string,cmpId:string){
    return this.http.get(this.apiUrl + '/site/listbyzone?zoneId='+zoneId+'&companyId='+cmpId).toPromise();
  }
  //Site Api
  addSite(name:string,address:string,coordinates:string,cmpId:string,userId:string,zoneId:string,radius:string){
    return this.http.get(this.apiUrl + '/site/addsite?name='+name+'&address='+address+'&coordinates='+coordinates+'&companyId='+cmpId+'&userId='+userId+'&zoneId='+zoneId+'&radius='+radius).toPromise();
  }
  updateSite(name:string,address:string,coordinates:string,cmpId:string,userId:string,radius:string,siteId:string){
    return this.http.get(this.apiUrl + '/site/editsite?name='+name+'&address='+address+'&coordinates='+coordinates+'&companyId='+cmpId+'&userId='+userId+'&radius='+radius+'&status='+status+'&siteId='+siteId).toPromise();
  }
  reactiveSite(siteId:string,cmpId:string,userId:string){
    return this.http.get(this.apiUrl + '/site/reactivatesite?siteId='+siteId+'&companyId='+cmpId+'&userId='+userId).toPromise();
  }
  deleteSite(siteId:string,cmpId:string,userId:string){
    return this.http.get(this.apiUrl + '/site/deletesite?siteId='+siteId+'&companyId='+cmpId+'&userId='+userId).toPromise();
  }
  siteList(cmpId:string){
    return this.http.get(this.apiUrl + '/site/list?companyId='+cmpId).toPromise();
  }
  //tasks Apis
  taskList(cmpId:string,userId:string){
    return this.http.get(this.apiUrl + '/opsuser/listforaddingtask?companyId='+cmpId+'&userId='+userId).toPromise();
  }
  //coordinates Api
  updateCoordinates(currentCoordinates:string,userId:string){
    return this.http.get(this.apiUrl + '/opsuser/updatecurrentcoordinates?currentCoordinates='+currentCoordinates+'&userId='+userId).toPromise();
  }
  audioRecord(meetingId: string){
    return this.http.get( this.apiUrl + '/blobtest/audiodetails?meetingId='+meetingId).toPromise();
  }
  audioAdd(meetingId: string, body: string){
    return this.http.post( this.apiUrl + '/blobtest/addaudiofile?meetingId='+meetingId,body).toPromise();
  }

}
