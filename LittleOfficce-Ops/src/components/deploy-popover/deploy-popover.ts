import { Component } from '@angular/core';

/**
 * Generated class for the DeployPopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'deploy-popover',
  templateUrl: 'deploy-popover.html'
})
export class DeployPopoverComponent {

  text: string;

  constructor() {
    console.log('Hello DeployPopoverComponent Component');
    this.text = 'Hello World';
  }

}
